import sys
from PyQt5.QtWidgets import (QWidget, QToolTip,
    QPushButton, QApplication)
from PyQt5 import QtGui,QtCore,QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont

from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
if is_pyqt5():
    from matplotlib.backends.backend_qt5agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
else:
    from matplotlib.backends.backend_qt4agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import pyqtgraph as pg
import numpy as np
import time
import pandas as pd
import warnings
import copy
import collections
import os
warnings.filterwarnings("ignore")

'''
Here I'm going to explain the main data structure, as it's somewhat complicated. 

self.filesWithSweeps = {filename: {key: [[keys],[curves],[children],[child2ren],[child3ren],[child4ren],[topLevelButtons]]}}

key = the string preceeding the numbers of a sweep. So for a group labelled "Sweep", all of the keys are [0,1,2,3...] and
are grouped this way for the colour map to only ever be able to map all of the same type of sweeps. If you're going to 
add in data for different things, that needs a different name so "Sweep" and "Diamond" can't be in the same place.

keys = [key1,key2,...] This holds the names of the groups in the hdf5 file. Could be extended to contain all paths by 
converting it to a dictionary where each key is a subfolder inside that. 

curves = {key: [[xVariable,yVariable,curve,live],[xData],[yData]}
xVariable and yVariable hold the currently selected contents of the corresponding combobox. It's easier than figuring 
out which combobox they belong to each time. 
curve is the graph line itself as a plot.plot object. I saw it called curve in a tutorial so I'm going with that. 
live is a boolean of if the data should be updated regularly or not. If not then static, if yes then live plot.
xData and yData are the current data for the curve, this can be updated regularly but is only used when curve.setData is 
called. This allows me to repeatedly check a file in case it's busy and then update the contents of the graph 
separately.
This is done to allow the graphing to be fairly smooth and not jerking with irregular amounts of data. 

children, child2ren, child3ren and child4ren are the comboboxes and checkboxes attacked to each sweep. For example, if 
the first key is "sweep5" then the 0th position will be the interface objects associated with that row. I choose 
"sweep5" just to say it's only based on key position in [keys], not to do with the name of the sweep. 
Having these here means I can reference them later as it seemed to be impossible to find them within the tree after 
creation otherwise. 

topLevelButtons = [setX,setY,setActive,setLive]
These are the two comboboxes and checkboxes seen on the top line of any loaded file. These are universal objects. If 
their contents or state are ever changed then all of the objects below them in the interface are changed to match. Try
not to accidentally change one if you've spent some time setting up which ones you want as this will overwrite 
everything.

There is an issue with the comboboxes in that I can't figure out how to stop scrolling on the mousewheel from taking 
control of them. If someone can figure that out then that would make the interface much nicer to use. In my opinion, at
least.
'''


class Example(QWidget):

    def __init__(self):
        super(Example,self).__init__()
        self.y = [0]
        self.x = [0]
        self.yvals = []
        self.xvals = []
        self.filepaths = []
        self.started = False
        self.liveStarted = False
        self.numberOfCurves = []
        self.curves = []
        self.filesWithSweeps = {}
        self.treeItems = 0
        self.testCount = 0
        self.data = []
        self.ignore = False

        self.initUI()


    def initUI(self):
        app = QtGui.QApplication([])
        self.static_canvas = FigureCanvas(Figure(figsize=(5, 3)))
        addFilebtn = QtGui.QPushButton("Add new file",self) #Opens up the file dialog for you to select your hdf5 file
        self.tree = QtGui.QTreeWidget() #This is the box at the bottom. It displays the filename followed by all the
                                        #sweeps in the file, it'll be explained better later
        self.walkCheckBox = QtGui.QCheckBox("Walk settings",self) #Sets the interface to walk settings down within each file

        #Headers of the tree so you know what each object refers to
        self.tree.headerItem().setText(0, "File")
        self.tree.headerItem().setText(1, "X")
        self.tree.headerItem().setText(2, "Y")
        self.tree.headerItem().setText(3, "Z")
        self.tree.headerItem().setText(4, "Active")
        self.tree.headerItem().setText(5, "Live")

        w = QtGui.QWidget()

        ## Create a grid layout to manage the widgets size and position. Six columns of 100 pixels each seems nice
        layout = QtGui.QGridLayout()
        w.setLayout(layout)
        for i in range(6):
            layout.setColumnMinimumWidth(i,100)

        ## Add widgets to the layout in their proper positions
        layout.addWidget(NavigationToolbar(self.static_canvas, self),1,0,1,6)
        layout.addWidget(self.static_canvas, 2, 0, 3, 6)  # plot goes on right side, spanning 3 rows and 6 columns
        layout.addWidget(addFilebtn,5,0,1,1)
        layout.addWidget(self.walkCheckBox,5,1,1,1)
        layout.addWidget(self.tree,6,0,3,6)

        ##Add button functions
        addFilebtn.clicked.connect(self.openFileNameDialog) #Opens file browser for you to select your file

        ## Display the widget as a new window
        w.show()

        ## Start the Qt event loop
        sys.exit(app.exec_())

    def createColourPlot(self):
        self._static_ax = self.static_canvas.figure.subplots()
        self._static_ax.pcolormesh(np.array(self.data[0]),np.array(self.data[1]), np.array(self.data[2]))
        self.im = plt.pcolormesh(np.array(self.data[0]),np.array(self.data[1]), np.array(self.data[2]))
        self.static_canvas.figure.colorbar(self.im)

        def format_coord(x, y):
            col = int(x)
            row = int(y)
            zVal = self.data[2][col][row]
            return 'x=%1.4f, y=%1.4f, z=%1.4f' % (x, y, zVal)

        self._static_ax.format_coord = format_coord
        self.static_canvas.draw()


    def populateTree(self):
        parent1 = QtGui.QTreeWidgetItem(self.tree) #Create a widget item, this is just a row in the tree view dedicated to this file
                                                  #This then can be expanded with each sweep taking up a row
        fileName = self.filepaths[len(self.filepaths)-1] #Uses the entire filepath to make sure files are unique
        parent1.setText(0, self.formatFileName(fileName))#I've cut out a lot of crap so you can see just the filename and none of the path.
                                                     #While it's not seen, the filepath is still used to make it unique

        for key in self.filesWithSweeps[fileName].keys():
            parent = QtGui.QTreeWidgetItem(parent1) #Create a widget item, this is just a row in the tree view dedicated to this file
                                                  #This then can be expanded with each sweep taking up a row
            text = key[1:] #Uses the entire filepath to make sure files are unique
            parent.setText(0, text)#I've cut out a lot of crap so you can see just the filename and none of the path.
                                                     #While it's not seen, the filepath is still used to make it unique

            #These four are stuck along with the parent file, this allows you to edit all sweeps at once
            setAllX = QtGui.QComboBox(self)
            setAllY = QtGui.QComboBox(self)
            setAllZ = QtGui.QComboBox(self)
            setAllActive = QtGui.QCheckBox(self)
            setAllLive = QtGui.QCheckBox(self)

            #Add those into the tree
            self.tree.setItemWidget(parent,1,setAllX)
            self.tree.setItemWidget(parent,2,setAllY)
            self.tree.setItemWidget(parent,3,setAllZ)
            self.tree.setItemWidget(parent,4,setAllActive)
            self.tree.setItemWidget(parent,5,setAllLive)

            #Here I'm adding them into filesWithSweeps to make sure they can be referenced externally as it's impossible to
            #find them otherwise.
            self.filesWithSweeps[fileName][key][7].append(setAllX)
            self.filesWithSweeps[fileName][key][7].append(setAllY)
            self.filesWithSweeps[fileName][key][7].append(setAllZ)
            self.filesWithSweeps[fileName][key][7].append(setAllActive)
            self.filesWithSweeps[fileName][key][7].append(setAllLive)

            #I'm attaching the functions with already set parameters to whenever any of the objects are interacted with.
            #I'm not sure if the deepcopy is required in the current version, I'll test it later and take it off if it's fine.
            self.filesWithSweeps[fileName][key][7][0].currentIndexChanged.connect(self.setAllLambda(fileName,key,2))
            self.filesWithSweeps[fileName][key][7][1].currentIndexChanged.connect(self.setAllLambda(fileName,key,3))
            self.filesWithSweeps[fileName][key][7][2].currentIndexChanged.connect(self.setAllLambda(fileName,key,4))
            self.filesWithSweeps[fileName][key][7][3].stateChanged.connect(self.setAllLambda(fileName,key,5))
            self.filesWithSweeps[fileName][key][7][4].stateChanged.connect(self.setAllLambda(fileName,key,6))

            #Could do it through lengths but it's unreadable as it is
            counter = 0

            #Iterates over all of the sweeps that are in a file and adds them in as new rows that expand under the filename
            #as their own separate rows.
            #As before, we have two comboboxes that will contain all of the variables within the sweeps. All of the sweeps
            #have their own variables, so if two have a mismatch it doesn't matter. However, with the mismatch any new
            #variables that are introduced are added into the top level comboboxes. So all will be seen in those.
            #The checkboxes do what you'd expect them to do. They set a graph active or live, or remove those settings.
            for key1 in self.filesWithSweeps[self.filepaths[len(self.filepaths)-1]][key][0]:
                root = QtGui.QTreeWidgetItem(parent, [key1])
                child = QtGui.QComboBox(self)
                child2 = QtGui.QComboBox(self)
                child3 = QtGui.QComboBox(self)
                child4 = QtGui.QCheckBox(self)
                child5 = QtGui.QCheckBox(self)
                self.tree.setItemWidget(root, 1, child)
                self.tree.setItemWidget(root, 2, child2)
                self.tree.setItemWidget(root, 3, child3)
                self.tree.setItemWidget(root, 4, child4)
                self.tree.setItemWidget(root, 5, child5)
                done = False
                while not done:
                    try:
                        for val in self.findColumnKeys(self.filepaths[len(self.filepaths)-1],key+key1+"/table"):
                            if val != "index":
                                child.addItem(val)
                                child2.addItem(val)
                                child3.addItem(val)
                                done = True
                                index = self.filesWithSweeps[fileName][key][7][0].findText(val, QtCore.Qt.MatchFixedString)
                                if index < 0:
                                    self.filesWithSweeps[fileName][key][7][0].addItem(val)
                                    self.filesWithSweeps[fileName][key][7][1].addItem(val)
                                    self.filesWithSweeps[fileName][key][7][2].addItem(val)
                    except:
                        pass

                #Adding them into the main data structure again for easy external referencing.
                self.filesWithSweeps[fileName][key][2].append(child)
                self.filesWithSweeps[fileName][key][3].append(child2)
                self.filesWithSweeps[fileName][key][4].append(child3)
                self.filesWithSweeps[fileName][key][5].append(child4)
                self.filesWithSweeps[fileName][key][6].append(child5)


                #Here we're only adding in the lambda for changing the graphing. The ones for changing variables are added
                #in once the graph lines exist for a sweep.
                self.filesWithSweeps[copy.deepcopy(fileName)][key][5][copy.deepcopy(counter)].stateChanged.connect(self.QCheckBoxActiveLambda(fileName,key,counter,key1))
                self.filesWithSweeps[copy.deepcopy(fileName)][key][6][copy.deepcopy(counter)].stateChanged.connect(self.QCheckBoxLiveLambda(fileName,key,counter,key1))
                counter+=1
            #Sets off a timer to check for if any new sweeps have appeared in the file
            self.checkForNewSweepsTimer()

    #Works exactly the same as above, except just appending a single row to the tree and data structures
    def addNewSweepToTree(self,filepath,key):
        position = self.filepaths.index(filepath)
        parent = self.tree.invisibleRootItem().child(position)
        root = QtGui.QTreeWidgetItem(parent, [key[1:]])
        child = QtGui.QComboBox(self)
        child2 = QtGui.QComboBox(self)
        child3 = QtGui.QComboBox(self)
        child4 = QtGui.QCheckBox(self)
        child5 = QtGui.QCheckBox(self)
        self.tree.setItemWidget(root, 1, child)
        self.tree.setItemWidget(root, 2, child2)
        self.tree.setItemWidget(root, 3, child3)
        self.tree.setItemWidget(root, 4, child4)
        self.tree.setItemWidget(root, 5, child5)
        done = False
        while not done:
            try:
                for val in self.findColumnKeys(filepath,key+"/table"):
                    if val != "index":
                        child.addItem(val)
                        child2.addItem(val)
                        child3.addItem(val)
                        done = True
            except:
                pass
        self.filesWithSweeps[filepath][key][2].append(child)
        self.filesWithSweeps[filepath][key][3].append(child2)
        self.filesWithSweeps[filepath][key][4].append(child3)
        self.filesWithSweeps[filepath][key][5].append(child4)
        self.filesWithSweeps[filepath][key][6].append(child5)
        boxPosition = len(self.filesWithSweeps[filepath][key][4])-1
        self.filesWithSweeps[filepath][5][boxPosition].stateChanged.connect(self.QCheckBoxActiveLambda(filepath,boxPosition,key))
        self.filesWithSweeps[filepath][6][boxPosition].stateChanged.connect(self.QCheckBoxLiveLambda(filepath,boxPosition,key))
        if self.walkCheckBox.checkState() == 2:
            walkRange = len(self.filesWithSweeps[filepath][key][2])-1
            for i in range(walkRange):
                self.filesWithSweeps[filepath][2][walkRange-i].setCurrentIndex(self.filesWithSweeps[filepath][2][walkRange-(i+1)].currentIndex())
                self.filesWithSweeps[filepath][3][walkRange-i].setCurrentIndex(self.filesWithSweeps[filepath][3][walkRange-(i+1)].currentIndex())
                self.filesWithSweeps[filepath][4][walkRange-i].setCurrentIndex(self.filesWithSweeps[filepath][4][walkRange-(i+1)].currentIndex())
                self.filesWithSweeps[filepath][5][walkRange-i].setCheckState(self.filesWithSweeps[filepath][5][walkRange-(i+1)].checkState())
                self.filesWithSweeps[filepath][6][walkRange-i].setCheckState(self.filesWithSweeps[filepath][6][walkRange-(i+1)].checkState())

    #Returns a lambda function, as the variables MUST be preset to allow every button to be unique and to have a button
    #attached to them. Trying to do this without lambdas results in them all being attached with non-unique functions,
    #pretty much every attempt left them either broken or referring to the wrong objects.
    def QComboBoxChangeLambda(self,fileName,key,counter,sweepName):
        return lambda: self.updateVariable(copy.deepcopy(fileName),copy.deepcopy(key),copy.deepcopy(counter))

    def setAllLambda(self,fileName,key,type):
        return lambda: self.setAll(copy.deepcopy(fileName),copy.deepcopy(key),copy.deepcopy(type))

    def QCheckBoxActiveLambda(self,fileName,key,counter,sweepName):
        return lambda: self.toggleActiveGraph(copy.deepcopy(fileName),copy.deepcopy(key),copy.deepcopy(counter),copy.deepcopy(sweepName))

    def QCheckBoxLiveLambda(self,fileName,key,counter,sweepName):
        return lambda: self.toggleLiveGraph(copy.deepcopy(fileName),copy.deepcopy(key),copy.deepcopy(counter),copy.deepcopy(sweepName))

    #Sets the existing sweeps in the data structure's X and Y variables to whatever has been selected. To force the graph
    #to update, I'm turning it off and on again. It works.
    def updateVariable(self,fileName,key,counter):
        self.filesWithSweeps[fileName][key][1][0] = self.filesWithSweeps[fileName][key][2][counter].currentText()
        self.filesWithSweeps[fileName][key][1][1] = self.filesWithSweeps[fileName][key][3][counter].currentText()
        self.filesWithSweeps[fileName][key][1][2] = self.filesWithSweeps[fileName][key][4][counter].currentText()
        check = self.filesWithSweeps[fileName][key][6][counter].checkState()
        if self.filesWithSweeps[fileName][key][5][counter].checkState() == 2:
            self.filesWithSweeps[fileName][key][5][counter].setCheckState(0)
            self.filesWithSweeps[fileName][key][6][counter].setCheckState(check)
            self.filesWithSweeps[fileName][key][5][counter].setCheckState(2)

    #Runs through the rest of the sweeps attached to a file and changes their current values to match the one that's just
    #been changed from the top level widget.
    def setAll(self,fileName,key,type):
        load = False
        if not self.ignore:
            for item in self.filesWithSweeps[fileName][key][type]:
                if type in [2,3,4]:
                    index = item.findText(self.filesWithSweeps[fileName][key][7][type-2].currentText(), QtCore.Qt.MatchFixedString)
                    if index >= 0:
                        item.setCurrentIndex(index)
                if type in [5,6]:
                    item.setCheckState(self.filesWithSweeps[fileName][key][7][type-2].checkState())
                    load = True
        if load:
            actives = []
            for i in range(len(self.filesWithSweeps[fileName][key][0])):
                if self.filesWithSweeps[fileName][key][5][i].checkState() == 2:
                    actives.append(i)
            self.static_canvas.figure.clear()
            if len(actives) > 0:
                self.load(fileName,key,self.filesWithSweeps[fileName][key][1][0],self.filesWithSweeps[fileName][key][1][1],self.filesWithSweeps[fileName][key][1][2],actives)
                self.createColourPlot()

    #Sets the graph to be active or not.
    #If it's being set to active then the curves section of the data structure has a new entry added into the dictionary
    #This is the sweep name and has the X and Y variable names attached to it, along wit the plot object and if it's live
    #or not. Whatever functions are attached to the comboboxes are removed and replaced with new ones. I can't figure out
    #a way of only adding it once, so I'm doing it every time. It doesn't slow it down perceptibly.
    #If it's being turned off then it sets live off and removes the data for the curve.
    def toggleActiveGraph(self,fileName,key,counter,sweepName):
        if self.filesWithSweeps[fileName][key][5][counter].checkState() == 2:
            self.filesWithSweeps[fileName][key][1] = []
            self.filesWithSweeps[fileName][key][1].append(self.filesWithSweeps[fileName][key][2][counter].currentText()) #X variable
            self.filesWithSweeps[fileName][key][1].append(self.filesWithSweeps[fileName][key][3][counter].currentText()) #Y variable
            self.filesWithSweeps[fileName][key][1].append(self.filesWithSweeps[fileName][key][4][counter].currentText()) #Y variable
            if self.filesWithSweeps[fileName][key][6][counter].checkState() == 2:
                self.filesWithSweeps[fileName][key][1].append(True)
            else:
                self.filesWithSweeps[fileName][key][1].append(False)
            if self.filesWithSweeps[fileName][key][7][3].checkState == 0:
                self.static_canvas.figure.clear()
                self.load(fileName,key,self.filesWithSweeps[fileName][key][1][0],self.filesWithSweeps[fileName][key][1][1],self.filesWithSweeps[fileName][key][1][2],[counter])
                self.createColourPlot()
            else:
                actives = []
                for i in range(len(self.filesWithSweeps[fileName][key][0])):
                    if self.filesWithSweeps[fileName][key][5][i].checkState() == 2:
                        actives.append(i)
                self.static_canvas.figure.clear()
                self.load(fileName,key,self.filesWithSweeps[fileName][key][1][0],self.filesWithSweeps[fileName][key][1][1],self.filesWithSweeps[fileName][key][1][2],actives)
                self.createColourPlot()
            try:
                self.filesWithSweeps[copy.deepcopy(fileName)][key][2][copy.deepcopy(counter)].currentIndexChanged.disconnect()
                self.filesWithSweeps[copy.deepcopy(fileName)][key][3][copy.deepcopy(counter)].currentIndexChanged.disconnect()
            except:
                pass
            #This referencing is awful.
            self.filesWithSweeps[copy.deepcopy(fileName)][key][2][copy.deepcopy(counter)].currentIndexChanged.connect(self.QComboBoxChangeLambda(fileName,key,counter,sweepName))
            self.filesWithSweeps[copy.deepcopy(fileName)][key][3][copy.deepcopy(counter)].currentIndexChanged.connect(self.QComboBoxChangeLambda(fileName,key,counter,sweepName))
        else:
            self.ignore = True
            self.filesWithSweeps[fileName][key][7][3].setCheckState(0)
            self.ignore = False
            actives = []
            for i in range(len(self.filesWithSweeps[fileName][key][0])):
                if self.filesWithSweeps[fileName][key][5][i].checkState() == 2:
                    actives.append(i)
            self.static_canvas.figure.clear()
            self.load(fileName,key,self.filesWithSweeps[fileName][key][1][0],self.filesWithSweeps[fileName][key][1][1],self.filesWithSweeps[fileName][key][1][2],actives)
            self.createColourPlot()

            try:
                self.filesWithSweeps[fileName][key][6][counter].setCheckState(0)
            except:
                pass

    #Sets the graph to active, and the live parameter to True. Then it sets the loader running to set the graph live.
    #Can also just set the live parameter to False so it doesn't update.
    def toggleLiveGraph(self,fileName,key,counter,sweepName):
        if self.filesWithSweeps[fileName][key][6][counter].checkState() == 2:
            if self.filesWithSweeps[fileName][key][5][counter].checkState() != 2:
                self.filesWithSweeps[fileName][key][5][counter].setCheckState(2)
            self.filesWithSweeps[fileName][key][1][3] = True
            if not self.liveStarted:
                self.setLoaderRunning()
        else:
            self.filesWithSweeps[fileName][key][1][3] = False

    #Opens up a file browser and returns the name of the file once one is selected. Nice of it to have that all ready.
    #Adds the name of the file to filepaths and creates a new entry in the data structure for that filename, and fills
    #in the necessary empty lists and dictionaries.
    #It then calls to add in a new tree structure from above.
    @QtCore.pyqtSlot()
    def openFileNameDialog(self):
        try:
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
            self.filepaths.append(fileName)
            self.filesWithSweeps[fileName] = self.returnSplitKeys(self.findSweepsInFile(fileName))
            for key in self.filesWithSweeps[fileName]:
                for i in range(7):
                    self.filesWithSweeps[fileName][key].append([]) #{filename:[[keys],[curves],[Xs],[Ys],[actives],[lives],[children]]}
            self.populateTree()
        except:
            pass

    def returnSplitKeys(self, keys):
        keyDict = {}
        for key in keys:
            for i in range(len(key)):
                if key[len(key)-(i+1)] not in [0,1,2,3,4,5,6,7,8,9]:
                    if not key[:len(key)-(i+1)] in keyDict.keys():
                        keyDict[key[:len(key)-(i+1)]] = [[]]
                        keyDict[key[:len(key)-(i+1)]][0].append(key[len(key)-(i+1):])
                    else:
                        keyDict[key[:len(key)-(i+1)]][0].append(key[len(key)-(i+1):])
                    break
        return keyDict

    #Sets to check the file for new sweeps added in every 500 milliseconds. Can be edited, but the smaller this is the
    #fewer live graphs it takes before the interface becomes really slow.
    @QtCore.pyqtSlot()
    def checkForNewSweepsTimer(self):
        self.started = True
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.checkForNewSweeps)
        self.timer.start(500)

    #Opens the given file, checks all the low-level entries and sees if there's anything new. If there is it will add it
    #and its dataset to the tree.
    def checkForNewSweeps(self):
        for fileName in self.filepaths:
            try:
                newKeys = self.findSweepsInFile(fileName)
                for key in newKeys:
                    if key in self.filesWithSweeps[fileName][0]:
                        pass
                    else:
                        self.filesWithSweeps[fileName][0].append(key)
                        self.addNewSweepToTree(fileName,key)
            except:
                pass

    #Gets rid of the filepath apart from the name of the file
    def formatFileName(self,fileName):
        for i in range(len(fileName)):
            if fileName[len(fileName)-(i+1)] == "/":
                return fileName[len(fileName)-(i):]

    #The initial call to the file to find out all of the top level groups, returns them as a list of keys. These are the
    #sweeps that are referred to throughout everywhere else.
    def findSweepsInFile(self,fileName):
        try:
            os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
        except:
            pass
        while True:
            if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                f = open(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt","w")
                f.close()

                if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                    pandaFile = pd.read_hdf(fileName,key="dataStruct") #Opens up the file and reads in the table
                    keys = list(pandaFile.get("name"))
                    for i in range(len(keys)):
                        keys[i] = "/"+keys[i]

                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    return keys
                else:
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    time.sleep(0.01)
            else:
                time.sleep(0.01)

    #Finds the variables within a dataset and returns them as a list. For the comboboxes.
    def findColumnKeys(self,fileName,keyPath):
        try:
            os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
        except:
            pass
        while 1:
            if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                f = open(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt","w")
                f.close()

                if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                    pandaFile = pd.read_hdf(fileName,key=keyPath)
                    keys = []
                    for key in pandaFile.keys():
                        keys.append(key)
                    f.close()
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    return keys
                else:
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    time.sleep(0.01)

            else:
                time.sleep(0.01)

    #Pulls the x and y data from the file with the requested variables. It'll find the file and the key is the sweep name
    def load(self,fileName,key,xVar,yVar,zVar,numbers):
        try:
            os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
        except:
            pass
        done = False
        while not done:
            if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                f = open(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt","w")
                f.close()
                if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                    x = []
                    y = []
                    z = []
                    for number in numbers:
                        pandaFile = pd.read_hdf(fileName,key = key+str(number)) #Opens up the file and reads in the table
                        pandaFile = pd.DataFrame(pandaFile) #Creates the new DataFrame
                        x.append(list(pandaFile.get(xVar)))
                        y.append(list(pandaFile.get(yVar)))
                        z.append(list(pandaFile.get(zVar)))
                        if len(numbers)<2:
                            x.append(list(pandaFile.get(xVar)))
                            y.append(list(pandaFile.get(yVar)))
                            z.append(list(pandaFile.get(zVar)))
                    self.data = [x,y,z]

                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    done = True
                else:
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    time.sleep(0.01)
            else:
                time.sleep(0.01)


    def loadLive(self,fileName,key,xVar,yVar,zVar):
        try:
            os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
        except:
            pass
        done = False
        while not done:
            if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                f = open(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt","w")
                f.close()
                if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                    x = []
                    y = []
                    z = []
                    pandaFile = pd.read_hdf(fileName,key = key+str(self.filesWithSweeps[fileName][key][0][len(self.filesWithSweeps[fileName][key][0])-1])) #Opens up the file and reads in the table
                    pandaFile = pd.DataFrame(pandaFile) #Creates the new DataFrame
                    x.append(list(pandaFile.get(xVar)))
                    y.append(list(pandaFile.get(yVar)))
                    z.append(list(pandaFile.get(zVar)))
                    while len(x)<len(self.data[0][len(self.data[0])-1]):
                        x.append(np.NAN)
                        y.append(np.NAN)
                        z.append(np.NAN)
                    self.data[0][len(self.data[0])-1] = x
                    self.data[0][len(self.data[0])-1] = y
                    self.data[0][len(self.data[0])-1] = z

                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    done = True
                else:
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    time.sleep(0.01)
            else:
                time.sleep(0.01)

    #Sets a timer to load data from all the old graphs every 500 milliseconds.
    @QtCore.pyqtSlot()
    def setLoaderRunning(self):
        self.timer3 = QtCore.QTimer()
        self.timer3.timeout.connect(self.loadLiveGraphs)
        self.timer3.start(500)
        self.liveStarted = True
        self.setLiveRunning()

    #Only updates the graphs with the new data every two seconds
    @QtCore.pyqtSlot()
    def setLiveRunning(self):
        self.timer4 = QtCore.QTimer()
        self.timer4.timeout.connect(self.setLiveGraphs)
        self.timer4.start(2000)

    #Attempts to pull in new data from the files for each existing value under "curves" in each filename for the data structure
    def loadLiveGraphs(self):
        for fileName in self.filepaths:
            for key in self.filesWithSweeps[fileName]:
                try:
                    if self.filesWithSweeps[fileName][key][1][3] == True:
                        self.loadLive(fileName,key,self.filesWithSweeps[fileName][key][1][0],self.filesWithSweeps[fileName][key][1][1],self.filesWithSweeps[fileName][key][1][2])
                except:
                    pass

    #Actually sets the curve's new data to the loaded data to update the view.
    def setLiveGraphs(self):
         for fileName in self.filepaths:
            for key in self.filesWithSweeps[fileName]:
                if self.filesWithSweeps[fileName][key][1][3] == True:
                    self.createColourPlot()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())

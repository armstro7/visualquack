import sys
from PyQt5.QtWidgets import (QWidget, QApplication)
from PyQt5 import QtGui,QtCore,QtWidgets
import pyqtgraph as pg
import pandas as pd
import warnings
import copy
import os
import time

#There are a tonne of read warnings that might come out from the file. They're not very important so at the moment they're
#just hidden.
warnings.filterwarnings("ignore")

'''
Here I'm going to explain the main data structure, as it's somewhat complicated. 

self.filesWithSweeps = {filename: [[keys],[curves],[children],[child2ren],[child3ren],[child4ren],[topLevelButtons]]}

keys = [key1,key2,...] This holds the names of the groups in the hdf5 file. Could be extended to contain all paths by 
converting it to a dictionary where each key is a subfolder inside that. 

curves = {key: [[xVariable,yVariable,curve,live],[xData],[yData]}
xVariable and yVariable hold the currently selected contents of the corresponding combobox. It's easier than figuring 
out which combobox they belong to each time. 
curve is the graph line itself as a plot.plot object. I saw it called curve in a tutorial so I'm going with that. 
live is a boolean of if the data should be updated regularly or not. If not then static, if yes then live plot.
xData and yData are the current data for the curve, this can be updated regularly but is only used when curve.setData is 
called. This allows me to repeatedly check a file in case it's busy and then update the contents of the graph 
separately.
This is done to allow the graphing to be fairly smooth and not jerking with irregular amounts of data. 

children, child2ren, child3ren and child4ren are the comboboxes and checkboxes attacked to each sweep. For example, if 
the first key is "sweep5" then the 0th position will be the interface objects associated with that row. I choose 
"sweep5" just to say it's only based on key position in [keys], not to do with the name of the sweep. 
Having these here means I can reference them later as it seemed to be impossible to find them within the tree after 
creation otherwise. 

topLevelButtons = [setX,setY,setActive,setLive]
These are the two comboboxes and checkboxes seen on the top line of any loaded file. These are universal objects. If 
their contents or state are ever changed then all of the objects below them in the interface are changed to match. Try
not to accidentally change one if you've spent some time setting up which ones you want as this will overwrite 
everything.

There is an issue with the comboboxes in that I can't figure out how to stop scrolling on the mousewheel from taking 
control of them. If someone can figure that out then that would make the interface much nicer to use. In my opinion, at
least.
'''


class HDF5InterfaceLoader(QWidget):

    def __init__(self):
        super(HDF5InterfaceLoader,self).__init__()
        self.filepaths = [] #Really it's just self.filesWithSweeps.keys() but I can't be bothered changing it.
        self.started = False #Makes sure a file has been selected and then starts off the timer looking for new sweeps
        self.liveStarted = False #If you select live at all this starts. I haven't put in to stop it yet, but it does nothing if there are no lives.
        self.filesWithSweeps = {} #Massive data structure: see above.

        self.initUI()

    #Actually creates and initialises the interface.
    def initUI(self):
        app = QtGui.QApplication([])
        self.plot = pg.PlotWidget() #Creates the view for all the graphs
        self.plot.enableAutoRange() #Makes sure on new data it'll change the axes to fit all data
        addFilebtn = QtGui.QPushButton("Add new file",self) #Opens up the file dialog for you to select your hdf5 file
        self.tree = QtGui.QTreeWidget() #This is the box at the bottom. It displays the filename followed by all the
                                        #sweeps in the file, it'll be explained better later
        self.walkCheckBox = QtGui.QCheckBox("Walk settings",self) #Sets the interface to walk settings down within each file

        #Headers of the tree so you know what each object refers to
        self.tree.headerItem().setText(0, "File")
        self.tree.headerItem().setText(1, "X")
        self.tree.headerItem().setText(2, "Y")
        self.tree.headerItem().setText(3, "Active")
        self.tree.headerItem().setText(4, "Live")

        w = QtGui.QWidget()

        ## Create a grid layout to manage the widgets size and position. Six columns of 100 pixels each seems nice
        layout = QtGui.QGridLayout()
        w.setLayout(layout)
        for i in range(6):
            layout.setColumnMinimumWidth(i,100)

        ## Add widgets to the layout in their proper positions
        layout.addWidget(self.plot, 1, 0, 3, 6)  # plot goes on right side, spanning 3 rows and 6 columns
        layout.addWidget(addFilebtn,4,0,1,1)
        layout.addWidget(self.walkCheckBox,4,1,1,1)
        layout.addWidget(self.tree,5,0,3,6)

        #Create actions
        addFilebtn.clicked.connect(self.openFileNameDialog) #Opens file browser for you to select your file
        #The walkCheckBox does get used when a new sweep is added. It checks the state of it and walks settings if it's
        #set to 2. This never ended up in a connect because it wasn't something a user would call, it was just a setting.

        ## Display the widget as a new window
        w.show()

        ## Start the Qt event loop
        app.exec_()

        #Doesn't take any parameters. It's always going to use the latest item of self.filepaths to populate
        #self.filesWithSweeps.
    def populateTree(self):
        parent = QtGui.QTreeWidgetItem(self.tree) #Create a widget item, this is just a row in the tree view dedicated to this file
                                                  #This then can be expanded with each sweep taking up a row
        fileName = self.filepaths[len(self.filepaths)-1] #Uses the entire filepath to make sure files are unique
        parent.setText(0, self.formatFileName(fileName))#I've cut out a lot of crap so you can see just the filename and none of the path.
                                                     #While it's not seen, the filepath is still used to make it unique

        #These four are stuck along with the parent file, this allows you to edit all sweeps at once
        setAllX = QtGui.QComboBox(self)
        setAllY = QtGui.QComboBox(self)
        setAllActive = QtGui.QCheckBox(self)
        setAllLive = QtGui.QCheckBox(self)

        #Add those into the tree
        self.tree.setItemWidget(parent,1,setAllX)
        self.tree.setItemWidget(parent,2,setAllY)
        self.tree.setItemWidget(parent,3,setAllActive)
        self.tree.setItemWidget(parent,4,setAllLive)

        #Here I'm adding them into filesWithSweeps to make sure they can be referenced externally as it's impossible to
        #find them otherwise.
        self.filesWithSweeps[fileName][6].append(setAllX)
        self.filesWithSweeps[fileName][6].append(setAllY)
        self.filesWithSweeps[fileName][6].append(setAllActive)
        self.filesWithSweeps[fileName][6].append(setAllLive)

        #I'm attaching the functions with already set parameters to whenever any of the objects are interacted with.
        #I'm not sure if the deepcopy is required in the current version, I'll test it later and take it off if it's fine.
        self.filesWithSweeps[fileName][6][0].currentIndexChanged.connect(self.setAllLambda(fileName,2))
        self.filesWithSweeps[fileName][6][1].currentIndexChanged.connect(self.setAllLambda(fileName,3))
        self.filesWithSweeps[fileName][6][2].stateChanged.connect(self.setAllLambda(fileName,4))
        self.filesWithSweeps[fileName][6][3].stateChanged.connect(self.setAllLambda(fileName,5))

        #Could do it through lengths but it's unreadable as it is
        counter = 0

        #Iterates over all of the sweeps that are in a file and adds them in as new rows that expand under the filename
        #as their own separate rows.
        #As before, we have two comboboxes that will contain all of the variables within the sweeps. All of the sweeps
        #have their own variables, so if two have a mismatch it doesn't matter. However, with the mismatch any new
        #variables that are introduced are added into the top level comboboxes. So all will be seen in those.
        #The checkboxes do what you'd expect them to do. They set a graph active or live, or remove those settings.
        for key in self.filesWithSweeps[fileName][0]:
            print key
            root = QtGui.QTreeWidgetItem(parent, [key[1:]])
            child = QtGui.QComboBox(self)
            child2 = QtGui.QComboBox(self)
            child3 = QtGui.QCheckBox(self)
            child4 = QtGui.QCheckBox(self)
            self.tree.setItemWidget(root, 1, child)
            self.tree.setItemWidget(root, 2, child2)
            self.tree.setItemWidget(root, 3, child3)
            self.tree.setItemWidget(root, 4, child4)
            done = False
            while not done:
                try:
                    for val in self.findColumnKeys(fileName,key+"/table"):
                        if val != "index":
                            child.addItem(val)
                            child2.addItem(val)
                            done = True
                            index = self.filesWithSweeps[fileName][6][0].findText(val, QtCore.Qt.MatchFixedString)
                            if index < 0:
                                self.filesWithSweeps[fileName][6][0].addItem(val)
                                self.filesWithSweeps[fileName][6][1].addItem(val)
                except:
                    pass

            #Adding them into the main data structure again for easy external referencing.
            self.filesWithSweeps[fileName][2].append(child)
            self.filesWithSweeps[fileName][3].append(child2)
            self.filesWithSweeps[fileName][4].append(child3)
            self.filesWithSweeps[fileName][5].append(child4)

            #Here we're only adding in the lambda for changing the graphing. The ones for changing variables are added
            #in once the graph lines exist for a sweep.
            self.filesWithSweeps[fileName][4][counter].stateChanged.connect(self.QCheckBoxActiveLambda(fileName,counter,key))
            self.filesWithSweeps[fileName][5][counter].stateChanged.connect(self.QCheckBoxLiveLambda(fileName,counter,key))
            counter+=1

        #Sets off a timer to check for if any new sweeps have appeared in the file
        self.checkForNewSweepsTimer()

    #Works exactly the same as above, except just appending a single row to the tree and data structures
    #fileName: the name of the file the new sweep is attached to, used to reference the right entry in self.filesWithSweeps
    #key: the name of the sweep that's being added.
    def addNewSweepToTree(self,fileName,key):
        position = self.filepaths.index(fileName)
        parent = self.tree.invisibleRootItem().child(position)
        root = QtGui.QTreeWidgetItem(parent, [key[1:]])
        child = QtGui.QComboBox(self)
        child2 = QtGui.QComboBox(self)
        child3 = QtGui.QCheckBox(self)
        child4 = QtGui.QCheckBox(self)
        self.tree.setItemWidget(root, 1, child)
        self.tree.setItemWidget(root, 2, child2)
        self.tree.setItemWidget(root, 3, child3)
        self.tree.setItemWidget(root, 4, child4)
        done = False
        while not done:
            try:
                for val in self.findColumnKeys(fileName,key+"/table"):
                    if val != "index": #Index is always added by pandas and doesn't work with append very well
                        child.addItem(val)
                        child2.addItem(val)
                        done = True
            except:
                pass
        self.filesWithSweeps[fileName][2].append(child)
        self.filesWithSweeps[fileName][3].append(child2)
        self.filesWithSweeps[fileName][4].append(child3)
        self.filesWithSweeps[fileName][5].append(child4)
        boxPosition = len(self.filesWithSweeps[fileName][4])-1
        self.filesWithSweeps[fileName][4][boxPosition].stateChanged.connect(self.QCheckBoxActiveLambda(fileName,boxPosition,key))
        self.filesWithSweeps[fileName][5][boxPosition].stateChanged.connect(self.QCheckBoxLiveLambda(fileName,boxPosition,key))

        #Here's where the walking comes into it. If the box is checked it will go through each sweep and the one above
        #will have its settings copied into the sweep below. The first sweep will just retain itself.
        if self.walkCheckBox.checkState() == 2:
            walkRange = len(self.filesWithSweeps[fileName][2])-1
            for i in range(walkRange):
                self.filesWithSweeps[fileName][2][walkRange-i].setCurrentIndex(self.filesWithSweeps[fileName][2][walkRange-(i+1)].currentIndex())
                self.filesWithSweeps[fileName][3][walkRange-i].setCurrentIndex(self.filesWithSweeps[fileName][3][walkRange-(i+1)].currentIndex())
                self.filesWithSweeps[fileName][4][walkRange-i].setCheckState(self.filesWithSweeps[fileName][4][walkRange-(i+1)].checkState())
                self.filesWithSweeps[fileName][5][walkRange-i].setCheckState(self.filesWithSweeps[fileName][5][walkRange-(i+1)].checkState())
        #Gets rid of any checked live boxes as they won't be appended to anymore. They're not disabled though just in
        #case you are appending to them again for some reason.
        for i in range(len(self.filesWithSweeps[fileName][5])-1):
            self.filesWithSweeps[fileName][5][i].setCheckState(0)

    #Returns a lambda function, as the variables MUST be preset to allow every button to be unique and to have a button
    #attached to them. Trying to do this without lambdas results in them all being attached with non-unique functions,
    #pretty much every attempt left them either broken or referring to the wrong objects.
    #Not certain copy.deepcopy() needs to be used here, but it works for now so I'll leave testing for later.

    #These three are the same for all of the functions below here
    #fileName: The name of the file it's come from, used to identify the entry in self.filesWithSweeps
    #counter, integer: How many entries deep it is. So the 8th row will have a counter = 7 (Python starts at 0)
    #sweepName: The name of the sweep. So "sweep0" or "diamond3"

    #These four return an anonymised lambda function to be attached to a certain action taken within the interface. It's
    #like returning a function but with preset variable values.
    def QComboBoxChangeLambda(self,fileName,counter,sweepName):
        return lambda: self.updateVariable(copy.deepcopy(fileName),copy.deepcopy(counter),copy.deepcopy(sweepName))

    #Type: An integer. 0 = X, 1 = Y, 2 = Active, 3 = Live
    def setAllLambda(self,fileName,type):
        return lambda: self.setAll(copy.deepcopy(fileName),copy.deepcopy(type))

    def QCheckBoxActiveLambda(self,fileName,counter,sweepName):
        return lambda: self.toggleActiveGraph(copy.deepcopy(fileName),copy.deepcopy(counter),copy.deepcopy(sweepName))

    def QCheckBoxLiveLambda(self,fileName,counter,sweepName):
        return lambda: self.toggleLiveGraph(copy.deepcopy(fileName),copy.deepcopy(counter),copy.deepcopy(sweepName))

    #Sets the existing sweeps in the data structure's X and Y variables to whatever has been selected. To force the graph
    #to update, I'm turning it off and on again. It works.
    def updateVariable(self,fileName,counter,sweepName):
        self.filesWithSweeps[fileName][1][sweepName][0] = self.filesWithSweeps[fileName][2][counter].currentText()
        self.filesWithSweeps[fileName][1][sweepName][1] = self.filesWithSweeps[fileName][3][counter].currentText()
        check = self.filesWithSweeps[fileName][5][counter].checkState()
        if self.filesWithSweeps[fileName][4][counter].checkState() == 2:
            self.filesWithSweeps[fileName][4][counter].setCheckState(0)
            self.filesWithSweeps[fileName][5][counter].setCheckState(check)
            self.filesWithSweeps[fileName][4][counter].setCheckState(2)

    #Runs through the rest of the sweeps attached to a file and changes their current values to match the one that's just
    #been changed from the top level widget.
    def setAll(self,fileName,type):
        for item in self.filesWithSweeps[fileName][type]:
            if type in [2,3]:
                index = item.findText(self.filesWithSweeps[fileName][6][type-2].currentText(), QtCore.Qt.MatchFixedString)
                if index >= 0:
                    item.setCurrentIndex(index)
            if type in [4,5]:
                item.setCheckState(self.filesWithSweeps[fileName][6][type-2].checkState())

    #Sets the graph to be active or not.
    #If it's being set to active then the curves section of the data structure has a new entry added into the dictionary
    #This is the sweep name and has the X and Y variable names attached to it, along wit the plot object and if it's live
    #or not. Whatever functions are attached to the comboboxes are removed and replaced with new ones. I can't figure out
    #a way of only adding it once, so I'm doing it every time. It doesn't slow it down perceptibly.
    #If it's being turned off then it sets live off and removes the data for the curve.
    def toggleActiveGraph(self,fileName,counter,sweepName):
        if self.filesWithSweeps[fileName][4][counter].checkState() == 2:
            self.filesWithSweeps[fileName][1][sweepName] = []
            self.filesWithSweeps[fileName][1][sweepName].append(self.filesWithSweeps[fileName][2][counter].currentText()) #X variable
            self.filesWithSweeps[fileName][1][sweepName].append(self.filesWithSweeps[fileName][3][counter].currentText()) #Y variable
            self.filesWithSweeps[fileName][1][sweepName].append(self.plot.plot(pen="y")) #The graph line
            if self.filesWithSweeps[fileName][5][counter].checkState() == 2:
                self.filesWithSweeps[fileName][1][sweepName].append(True)
            else:
                self.filesWithSweeps[fileName][1][sweepName].append(False)
                data = self.load(fileName,sweepName+"/table",self.filesWithSweeps[fileName][1][sweepName][0],
                                                                                  self.filesWithSweeps[fileName][1][sweepName][1])
                self.filesWithSweeps[fileName][1][sweepName][2].setData(x = data[0],y = data[1])
                self.plot.setLabels(left=self.filesWithSweeps[fileName][1][sweepName][0],bottom=self.filesWithSweeps[fileName][1][sweepName][1])
            self.filesWithSweeps[fileName][1][sweepName].append([]) #X data
            self.filesWithSweeps[fileName][1][sweepName].append([]) #Y data

            #Removes attached functions because if I just connected it would be adding it on so it would be doing the
            #function twice instead of replacing it.
            try:
                self.filesWithSweeps[fileName][2][counter].currentIndexChanged.disconnect()
                self.filesWithSweeps[fileName][3][counter].currentIndexChanged.disconnect()
            except:
                pass
            self.filesWithSweeps[fileName][2][counter].currentIndexChanged.connect(self.QComboBoxChangeLambda(fileName,counter,sweepName))
            self.filesWithSweeps[fileName][3][counter].currentIndexChanged.connect(self.QComboBoxChangeLambda(fileName,counter,sweepName))
        else:
            try:
                self.filesWithSweeps[fileName][5][counter].setCheckState(0)
            except:
                pass
            #Easier to set the data empty than remove it completely.
            self.filesWithSweeps[fileName][1][sweepName][2].setData(x=[],y=[])
            self.plot.setLabels(left="",bottom="")

    #Sets the graph to active, and the live parameter to True. Then it sets the loader running to set the graph live.
    #Can also just set the live parameter to False so it doesn't update.
    def toggleLiveGraph(self,fileName,counter,sweepName):
        if counter == len(self.filesWithSweeps[fileName][5])-1:
            if self.filesWithSweeps[fileName][5][counter].checkState() == 2:
                if self.filesWithSweeps[fileName][4][counter].checkState() != 2:
                    self.filesWithSweeps[fileName][4][counter].setCheckState(2)
                self.filesWithSweeps[fileName][1][sweepName][3] = True
                if not self.liveStarted:
                    self.setLoaderRunning()
            else:
                self.filesWithSweeps[fileName][1][sweepName][3] = False
        else:
            self.filesWithSweeps[fileName][5][counter].setCheckState(0)
            try:
                self.filesWithSweeps[fileName][1][sweepName][3] = False
            except:
                pass

    #Opens up a file browser and returns the name of the file once one is selected. Nice of it to have that all ready.
    #Adds the name of the file to filepaths and creates a new entry in the data structure for that filename, and fills
    #in the necessary empty lists and dictionaries.
    #It then calls to add in a new tree structure from above.
    @QtCore.pyqtSlot()
    def openFileNameDialog(self):
        try:
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
            self.filepaths.append(fileName)
            self.filesWithSweeps[fileName] = []
            self.filesWithSweeps[fileName].append(self.findSweepsInFile(fileName)) #Holds the keys of the file
            self.filesWithSweeps[fileName].append({})
            for i in range(5):
                self.filesWithSweeps[fileName].append([]) #{filename:[[keys],[curves],[Xs],[Ys],[actives],[lives],[children]]}
            self.populateTree()
        except:
            pass

    #Sets to check the file for new sweeps added in every 500 milliseconds. Can be edited, but the smaller this is the
    #fewer live graphs it takes before the interface becomes really slow.
    #Edit: This can now probably be sped up if needed as the interface only allows one live sweep.
    @QtCore.pyqtSlot()
    def checkForNewSweepsTimer(self):
        self.started = True
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.checkForNewSweeps)
        self.timer.start(500)

    #Opens the given file, checks all the low-level entries and sees if there's anything new. If there is it will add it
    #and its dataset to the tree.
    def checkForNewSweeps(self):
        for fileName in self.filepaths:
            try:
                newKeys = self.findSweepsInFile(fileName)
                for key in newKeys:
                    if key in self.filesWithSweeps[fileName][0]:
                        pass
                    else:
                        self.filesWithSweeps[fileName][0].append(key)
                        self.addNewSweepToTree(fileName,key)
            except:
                pass

    #Gets rid of the filepath apart from the name of the file
    #Returns just the file name without the rest of the path.
    def formatFileName(self,fileName):
        for i in range(len(fileName)):
            if fileName[len(fileName)-(i+1)] == "/":
                return fileName[len(fileName)-(i):]

    #For the three methods below there is now a lock file system in place to make sure there are no crashes, as the more
    #columns a dataset has the slower it is to load from the file. This is also why there is a new dataset in each hdf5
    #file called "dataStruct". This holds useful information about the file, such as the available sweeps. Normally this
    #could be done with pandafile.keys(), but it turns out that this is insanely slow with larger numbers of columns.
    #Importantly, this needs to be paired with the HDF5Saver if you're going to be plotting things live.

    #The saver will put down a file that the graphing system can see and if it exists the graphing cannot touch the file.
    #The same thing happens the other way around just in case you try to save as we're reading, but it shouldn't be doing
    #that particularly often, so it shouldn't cause much if any slowdown.

    #The initial call to the file to find out all of the top level groups, returns them as a list of keys. These are the
    #sweeps that are referred to throughout everywhere else.
    #Returns a list
    def findSweepsInFile(self,fileName):
        try:
            os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
        except:
            pass
        while True:
            if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                f = open(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt","w")
                f.close()

                if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                    pandaFile = pd.read_hdf(fileName,key="dataStruct") #Opens up the file and reads in the table
                    keys = list(pandaFile.get("name"))
                    for i in range(len(keys)):
                        keys[i] = "/"+keys[i]

                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    return keys
                else:
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    time.sleep(0.01)
            else:
                time.sleep(0.01)

    #Finds the variables within a dataset and returns them as a list. For the comboboxes.
    #Returns a list
    def findColumnKeys(self,fileName,keyPath):
        try:
            os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
        except:
            pass
        while 1:
            if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                f = open(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt","w")
                f.close()

                if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                    pandaFile = pd.read_hdf(fileName,key=keyPath)
                    keys = []
                    for key in pandaFile.keys():
                        keys.append(key)
                    f.close()
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    return keys
                else:
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    time.sleep(0.01)

            else:
                time.sleep(0.01)

    #Pulls the x and y data from the file with the requested variables. It'll find the file and the key is the sweep name
    #fileName: the name of the file
    #key: the name of the dataset in the file
    #xVar: the name of the x variable you're pulling out of the dataset.
    #yVar: the name of the y variable you're pulling out of the dataset.
    #Returns a list of two lists of whatever is in the dataset. Please don't save strings, this will break it. 
    def load(self,fileName,key,xVar,yVar):
        try:
            os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
        except:
            pass
        while 1:
            if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                f = open(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt","w")
                f.close()
                if not os.path.exists(os.getcwd()+"\hold\\"+self.formatFileName(fileName)[:-3]+".txt"):
                    pandaFile = pd.read_hdf(fileName,key = key) #Opens up the file and reads in the table
                    pandaFile = pd.DataFrame(pandaFile) #Creates the new DataFrame
                    x = list(pandaFile.get(xVar))
                    y = list(pandaFile.get(yVar))
                    f.close()
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    return [x,y]
                else:
                    os.remove(os.getcwd()+"\hold\\graph"+self.formatFileName(fileName)[:-3]+".txt")
                    time.sleep(0.01)
            else:
                time.sleep(0.01)


    #Sets a timer to load data from all the old graphs every 500 milliseconds.
    @QtCore.pyqtSlot()
    def setLoaderRunning(self):
        self.timer3 = QtCore.QTimer()
        self.timer3.timeout.connect(self.loadLiveGraphs)
        self.timer3.start(500)
        self.liveStarted = True
        self.setLiveRunning()

    #Only updates the graphs with the new data every two seconds
    @QtCore.pyqtSlot()
    def setLiveRunning(self):
        self.timer4 = QtCore.QTimer()
        self.timer4.timeout.connect(self.setLiveGraphs)
        self.timer4.start(2000)

    #Attempts to pull in new data from the files for each existing value under "curves" in each filename for the data structure
    def loadLiveGraphs(self):
        for fileName in self.filepaths:
            for sweep in self.filesWithSweeps[fileName][1]:
                try:
                    if self.filesWithSweeps[fileName][1][sweep][3] == True:
                        #At the moment all filenames are called "table". I should check what they're called beforehand
                        #to remove this limitation, but eh.
                        data = self.load(fileName,sweep+"/table",self.filesWithSweeps[fileName][1][sweep][0],self.filesWithSweeps[fileName][1][sweep][1])
                        self.filesWithSweeps[fileName][1][sweep][4] = data[0]
                        self.filesWithSweeps[fileName][1][sweep][5] = data[1]
                except:
                    pass

    #Actually sets the curve's new data to the loaded data to update the view.
    def setLiveGraphs(self):
         for fileName in self.filepaths:
            for sweep in self.filesWithSweeps[fileName][1]:
                if self.filesWithSweeps[fileName][1][sweep][3] == True:
                    self.filesWithSweeps[fileName][1][sweep][2].setData(x=self.filesWithSweeps[fileName][1][sweep][4],y=self.filesWithSweeps[fileName][1][sweep][5])
                    self.plot.setLabels(left=self.filesWithSweeps[fileName][1][sweep][0],bottom=self.filesWithSweeps[fileName][1][sweep][1])


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = HDF5InterfaceLoader()
    sys.exit(app.exec_())

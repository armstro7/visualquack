import sys
from PyQt5.QtWidgets import (QWidget, QToolTip,
    QPushButton, QApplication)
from PyQt5 import QtGui,QtCore,QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
import pyqtgraph as pg
import numpy as np
import time
import pandas as pd
import warnings
import copy
import collections
warnings.filterwarnings("ignore")

'''
Here I'm going to explain the main data structure, as it's somewhat complicated. 

self.filesWithSweeps = {filename: [[keys],[curves],[children],[child2ren],[child3ren],[child4ren],[topLevelButtons]]}

keys = [key1,key2,...] This holds the names of the groups in the hdf5 file. Could be extended to contain all paths by 
converting it to a dictionary where each key is a subfolder inside that. 

curves = {key: [[xVariable,yVariable,curve,live],[xData],[yData]}
xVariable and yVariable hold the currently selected contents of the corresponding combobox. It's easier than figuring 
out which combobox they belong to each time. 
curve is the graph line itself as a plot.plot object. I saw it called curve in a tutorial so I'm going with that. 
live is a boolean of if the data should be updated regularly or not. If not then static, if yes then live plot.
xData and yData are the current data for the curve, this can be updated regularly but is only used when curve.setData is 
called. This allows me to repeatedly check a file in case it's busy and then update the contents of the graph 
separately.
This is done to allow the graphing to be fairly smooth and not jerking with irregular amounts of data. 

children, child2ren, child3ren and child4ren are the comboboxes and checkboxes attacked to each sweep. For example, if 
the first key is "sweep5" then the 0th position will be the interface objects associated with that row. I choose 
"sweep5" just to say it's only based on key position in [keys], not to do with the name of the sweep. 
Having these here means I can reference them later as it seemed to be impossible to find them within the tree after 
creation otherwise. 

topLevelButtons = [setX,setY,setActive,setLive]
These are the two comboboxes and checkboxes seen on the top line of any loaded file. These are universal objects. If 
their contents or state are ever changed then all of the objects below them in the interface are changed to match. Try
not to accidentally change one if you've spent some time setting up which ones you want as this will overwrite 
everything.

There is an issue with the comboboxes in that I can't figure out how to stop scrolling on the mousewheel from taking 
control of them. If someone can figure that out then that would make the interface much nicer to use. In my opinion, at
least.
'''


class Example(QWidget):

    def __init__(self):
        super(Example,self).__init__()
        self.y = [0]
        self.x = [0]
        self.yvals = []
        self.xvals = []
        self.filepaths = []
        self.started = False
        self.liveStarted = False
        self.numberOfCurves = []
        self.curves = []
        self.filesWithSweeps = {}
        self.treeItems = 0
        self.testCount = 0

        self.initUI()


    def initUI(self):
        app = QtGui.QApplication([])
        self.plot = pg.PlotWidget() #Creates the view for all the graphs
        self.plot.enableAutoRange() #Makes sure on new data it'll change the axes to fit all data
        addFilebtn = QtGui.QPushButton("Add new file",self) #Opens up the file dialog for you to select your hdf5 file
        self.tree = QtGui.QTreeWidget() #This is the box at the bottom. It displays the filename followed by all the
                                        #sweeps in the file, it'll be explained better later
        self.walkCheckBox = QtGui.QCheckBox("Walk settings",self) #Sets the interface to walk settings down within each file

        #Headers of the tree so you know what each object refers to
        self.tree.headerItem().setText(0, "File")
        self.tree.headerItem().setText(1, "X")
        self.tree.headerItem().setText(2, "Y")
        self.tree.headerItem().setText(3, "Active")
        self.tree.headerItem().setText(4, "Live")

        w = QtGui.QWidget()

        ## Create a grid layout to manage the widgets size and position. Six columns of 100 pixels each seems nice
        layout = QtGui.QGridLayout()
        w.setLayout(layout)
        for i in range(6):
            layout.setColumnMinimumWidth(i,100)

        data = []
        for i in range(100):
            data.append([])
            for j in range(100):
                data[i].append(j+float(np.random.randint(0,100000))/100000)

        data = np.array(data)

        pos = np.array([0.0, 0.33, 0.66,  1.0])
        color = np.array([[0,255,255,255], [0,172,255,255], [0,86,255,255], [0,0,255,255]], dtype=np.ubyte)
        cmap = pg.ColorMap(pos, color)

        ## Add widgets to the layout in their proper positions
        layout.addWidget(self.plot, 1, 0, 3, 6)  # plot goes on right side, spanning 3 rows and 6 columns
        layout.addWidget(addFilebtn,4,0,1,1)
        layout.addWidget(self.walkCheckBox,4,1,1,1)
        layout.addWidget(self.tree,5,0,3,6)

        self.img = pg.ImageItem()
        self.img.setLookupTable(cmap.getLookupTable(0,1,100))
        self.img.setLevels([0,1])

        self.plot.addItem(self.img)
        print data.T
        self.img.setImage(data.T)

        #Create actions
        #addFilebtn.clicked.connect(self.openFileNameDialog) #Opens file browser for you to select your file
        #walkCheckBox.stateChanged.connect(self.setWalk) #Will allow settings to walk down sweeps when implemented




        ## Display the widget as a new window
        w.show()

        ## Start the Qt event loop
        app.exec_()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())

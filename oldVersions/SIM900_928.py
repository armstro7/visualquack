# based on the work by
# Pieter de Groot <pieterdegroot@gmail.com>, 2008
# Martijn Schaafsma <qtlab@mcschaafsma.nl>, 2008
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from instrument import Instrument
from qkit import visa
import types
import time
import logging
import numpy

class SIM900_928(Instrument):
    '''
    This is the driver for the SIM900 rack with SIM928 modules mounted

    Usage:
    Initialize with
    <name> = instruments.create('<name>', 'SIM900_928', address='<GBIP address>, reset=<bool>')
    '''

    def __init__(self, name, address, reset=False):
        '''
        Initializes the SIM900 and sees if there are any SIM928s attached, and communicates with the wrapper.

        Input:
          name (string)    : name of the instrument
          address (string) : GPIB address
          reset (bool)     : resets to default values, default=False
        '''
        self.name = name
        logging.info(__name__ + ' : Initializing instrument SIM900 via GPIB')
        Instrument.__init__(self, name, tags=['physical'])

        # Add some global constants
        self._address = address
        self._visainstrument = visa.instrument(self._address)
        print self._visainstrument
        self.initialised = False

        self.ports = [False,False,False,False,False,False,False,False]

        self._initialise()

        self.defaultSIM928Max = 20
        self.defaultSIM928Min = -20

        self.portMaxValues = [20,20,20,20,20,20,20,20]
        self.portMinValues = [-20,-20,-20,-20,-20,-20,-20,-20]

    #Check and initialise all ports before running, if this fails then there's nothing available to use
    def _initialise(self):
        for i in range(8):
            #Opens the port and checks what ID it gives out to know if they're SIM928s or if the buffer is broken
            self.initialise(str(i+1))
            returnedValue = self.getName(str(i+1))

            if "SIM928" in returnedValue:
                self.ports[i] = "SIM928"
            else:
                print returnedValue
            print "Device in port " +str(i+1)+ " is " + str(self.ports[i])
            time.sleep(60)

        #A little thing to save time. No point trying to contact non-existent ports
        working = False
        for port in self.ports:
            if port is not None:
                working = True
        if not working:
            #This apparently works if all fails
            print "No devices appear to be connected. The device may be struggling with its buffer.\nTry sending some commands through NI MAX to reset this"

    #Opens the port to allow reading/writing
    def initialise(self,port):
        command = "SNDT " + port + ", \"OPON\""
        self.write(port,command)

    #Returns the port's ID, will either be nothing or SIM928 usually
    def getName(self,port):
        command = "SNDT " + port + ", \"*IDN?\""
        return self.query(port,command)

    #Voltages must be to three decimal places, so this forces that
    def setVoltage(self,port,value):
        if value <= self.portMaxValues[int(port)-1] and value >= self.portMinValues[int(port)-1]
        command = "SNDT " + port + ", \"VOLT " + '%.3f' % value + "\" "
        self.write(port, command)

    def setVoltageMin(self,port,value):
        if value >= self.defaultSIM928Min and value <= self.defaultSIM928Max:
            self.portMinValues[int(port)-1] = value

    def setVoltageMax(self,port,value):
        if value >= self.defaultSIM928Min and value <= self.defaultSIM928Max:
            self.portMaxValues[int(port)-1] = value


    #Writing the commands to the device, requires a bunch of attempts in case it bork.
    #If the port is open and this stops working then you'll have to manually send commands through NI MAX in order to
    #reset the buffer. It's not great.
    def write(self,port,command):
        done = False
        for i in range(20):
            try:
                self._visainstrument.write("FLSH " + port)
                self._visainstrument.write(command)
                print "written"

                for j in range(100):
                    if self._visainstrument.query("NOUT? " +port) == "0\n": #Check no bytes are left to be read.
                        print "Write successful"
                        done = True
                        return

                self._visainstrument.write("SRST " + port) #Theoretically resets the port if it breaks. Sometimes works.
                time.sleep(2)
            except:
                self._visainstrument.write("SRST " + port)
                time.sleep(2)

        if not done:
            print("Failed to write correctly")


    def getVoltage(self,port):
        command = "SNDT " + port + ", \"VOLT?\""
        self.query(port,command)

    def query(self,port,command):
        if not self.initialised or self.ports[int(port)-1] is not False:
            for i in range(20):
                try:
                    self._visainstrument.write("FLSH " + port)
                    self._visainstrument.write(command)
                    previousResponse = "Why"

                    for j in range(100):
                        response = self._visainstrument.query("NINP? " + port) #Checks how many bytes can be read
                        if response == previousResponse and not response == "0\n":
                            response = response[:-1]
                            #Reads the number of bytes above plus the ones that the device likes to push onto the end
                            response = self._visainstrument.query("GETN? " + port + "," + str(int(response) + 7))
                            print response
                            #Clears off excess characters pushed by the device
                            if len(response)>5:
                                response = response[5:]

                            else:
                                response = ""

                            return response

                        previousResponse = response

                    self._visainstrument.write("SRST " + port)
                    time.sleep(2)
                except:
                    self._visainstrument.write("SRST " + port)
                    time.sleep(2)
            return ""
        else:
            print "Device is not initialised. Request sent to empty port"
            return ""
import pandas as pd
import os
import HDF5Saver

sweeps = []
def stuff(data,location):
    tableStructure = {}
    for key in data.keys():
        tableStructure[key] = [key]
        if type(data[key]) is not list:
            data[key] = [data[key]]

    df = pd.DataFrame(data)
    struct = {"name": [location]}
    dfStruct = pd.DataFrame(struct)

    pandaFile = pd.HDFStore("testing.h5",mode="a")

    if location not in sweeps:
        sweeps.append(location)
        pandaFile.append("dataStruct",dfStruct,format="table",data_columns = True)


    pandaFile.append(location, df, format='table', data_columns = True)
    pandaFile.close()

try:
    os.remove("testing.h5")
except:
    pass
for i in range(10):
    data = {'data': "Henlo", 'count': i}
    stuff(data,"sweep"+str(i))

pandaFile = pd.read_hdf("testing.h5",key="dataStruct")
print list(pandaFile.get("name"))
#print pandaFile["name"]
for key in sweeps:
    pandaFile = pd.read_hdf("testing.h5",key=key)
    print pandaFile

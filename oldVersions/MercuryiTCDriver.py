self._host = host
self._port = port
self._soc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
self._soc.connect((self._host, self._port))

'''function [T1K, TPT2, THS] = read_MiTC(dev)
    fopen(dev);
    
    fprintf(dev, 'READ:DEV:MB1.T1:TEMP:SIG:TEMP');
    pause(0.1)
    T1K = fscanf(dev, 'STAT:DEV:MB1.T1:TEMP:SIG:TEMP:%fK\n');
    pause(0.1)
    fprintf(dev, 'READ:DEV:DB6.T1:TEMP:SIG:TEMP');
    pause(0.1)
    TPT2 = fscanf(dev, 'STAT:DEV:DB6.T1:TEMP:SIG:TEMP:%fK\n');
    pause(0.1)
    fprintf(dev, 'READ:DEV:DB7.T1:TEMP:SIG:TEMP');
    pause(0.1)
    THS = fscanf(dev, 'STAT:DEV:DB7.T1:TEMP:SIG:TEMP:%fK\n');  
    pause(0.1)
    
    fclose(dev);
end'''

This becomes

write("READ:DEV:MB1.T1:TEMP:SIG:TEMP")
time.sleep(0.1)
T1K = query("STAT:DEV:MB1.T1:TEMP:SIG:TEMP:%fK\n")
time.sleep(0.1)
write('READ:DEV:DB6.T1:TEMP:SIG:TEMP')

etc.

# Keithley 2636A class, to perform the communication between the Wrapper and the device
# Hannes Rotzinger, hannes.rotzinger@kit.edu 2010
#
# based on the work by
# Pieter de Groot <pieterdegroot@gmail.com>, 2008
# Martijn Schaafsma <qtlab@mcschaafsma.nl>, 2008
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from instrument import Instrument
from qkit import visa
import types
import time
import logging
import numpy

class SIM900(Instrument):
    '''
    This is the driver for the Keithley 2636A Source Meter

    Usage:
    Initialize with
    <name> = instruments.create('<name>', 'Keithley_2636A', address='<GBIP address>, reset=<bool>')
    '''

    def __init__(self, name, address, reset=False):
        '''
        Initializes the Keithley_2000, and communicates with the wrapper.

        Input:
          name (string)    : name of the instrument
          address (string) : GPIB address
          reset (bool)     : resets to default values, default=False
        '''
        self.name = name
        logging.info(__name__ + ' : Initializing instrument SIM900 via GPIB')
        Instrument.__init__(self, name, tags=['physical'])

        # Add some global constants
        self._address = address
        self._visainstrument = visa.instrument(self._address)
        print self._visainstrument

        self.ports = [False,False,False,False,False,False,False,False]

        self.add_function('reset')

        if (reset):
            self.reset()
        else:
            self.get_all()

    def initialise(self,port):
        command = "SNDT " + port + ", \"OPON\""
        self.write(port,command)

    def getName(self,port):
        command = "SNDT " + port + ", \"*IDN?\""
        return self.query(port,command)

    def setVoltage(self,port,value):
        command = "SNDT " + port + ", \"VOLT " + '%.3f' % value + "\" "
        self.write(port, command)


    #Writing the commands to the device, requires a bunch of attempts in case it bork.
    def write(self,port,command):
        done = False
        for i in range(20):
            try:
                self._visainstrument.write("FLSH " + port)
                self._visainstrument.write(command)
                print "written"

                for j in range(100):
                    if self._visainstrument.query("NOUT? " +port) == "0\n": #Check no bytes are left to be read.
                        print "Write successful"
                        done = True
                        return

                self._visainstrument.write("SRST " + port)
                time.sleep(2)
            except:
                self._visainstrument.write("SRST " + port)
                time.sleep(2)

        if not done:
            print("Failed to write correctly")


    def getVoltage(self,port):
        command = "SNDT " + port + ", \"VOLT?\""
        return self.query(port,command)

    def query(self,port,command):
        for i in range(20):
            try:
                self._visainstrument.write("FLSH " + port)
                self._visainstrument.write(command)
                previousResponse = "Why"

                for j in range(100):
                    response = self._visainstrument.query("NINP? " + port)
                    if response == previousResponse and not response == "0\n":
                        response = response[:-1]
                        response = self._visainstrument.query("GETN? " + port + "," + str(int(response) + 7))

                        print response
                        if len(response)>5:
                            response = response[5:]
                        else:
                            response = ""

                        return response

                    previousResponse = response

                self._visainstrument.write("SRST " + port)
                time.sleep(2)
            except:
                self._visainstrument.write("SRST " + port)
                time.sleep(2)

        return ""

    def reset(self):
        '''
        Resets the instrument to default values

        Input:
            None

        Output:
            None
        '''
        logging.info(__name__ + ' : resetting instrument')
        self._visainstrument.write("*RST")
        #self.get_all()

    def get_all(self):
        '''
        Reads all implemented parameters from the instrument,
        and updates the wrapper.

        Input:
            None

        Output:
            None
        '''
        logging.info(__name__ + ' : get all')
        #self.do_get_voltageA()
        #self.get_voltageB()
        #self.get_currentA()
        #self.get_currentB()



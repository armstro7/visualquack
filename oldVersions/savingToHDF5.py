from tables import *
import numpy as np
import os
import pandas as pd
import thread
import time

def stuff(x,times,file):
    if times <4:
        location = "Sweep"+str(times)
    else:
        location = "diamond"+str(times)

    data = {'data': [np.sin(x)], 'count': [x],'data1': [np.sin(x)], 'count1': [x],'data2': [np.sin(x)], 'count2': [x],'data3': [np.sin(x)], 'count3': [x],
            'data4': [np.sin(x)], 'count4': [x],'data5': [np.sin(x)], 'count5': [x],'data6': [np.sin(x)], 'count6': [x],'data7': [np.sin(x)], 'count7': [x],
            'data8': [np.sin(x)], 'count8': [x],'data9': [np.sin(x)], 'count9': [x],'data10': [np.sin(x)], 'count10': [x],'data11': [np.sin(x)], 'count11': [x]}
    df = pd.DataFrame(data)

    f = open(files[file],"w")
    done = False
    print holdFiles[file]
    print os.path.exists(holdFiles[file])
    while not done:
        time.sleep(0.01)
        if not os.path.exists(passFiles[file]):
            hdfs[file].append(location, df, format='table', data_columns = True)
            done = True
        else:
            time.sleep(0.01)
    if os.path.exists(holdFiles[file]):
        g = open(passFiles[file],"w")
        g.close()
    f.close()
    print location
    print files[file]
    os.remove(files[file])
    print "Saved"
    time.sleep(0.5)


files = []
holdFiles = []
passFiles = []
hdfs = []
for i in range(3):
    try:
        os.remove("chaseCheck"+str(i)+".h5")
    except:
        pass
    #files.append(open_file("chaseCheck"+str(i)+".h5", mode='a')) #Opens the main file

    if not os.path.exists(os.getcwd()+"\hold\\"):
        os.makedirs(os.getcwd()+"\hold\\")
    files.append(os.getcwd()+"\hold\chaseCheck"+str(i)+".txt")
    holdFiles.append(os.getcwd()+"\hold\graphchaseCheck"+str(i)+".txt")
    passFiles.append(os.getcwd()+"\hold\passchaseCheck"+str(i)+".txt")
    hdfs.append(pd.HDFStore("chaseCheck"+str(i)+".h5",mode="a")) #Opens up an HDFStore object for dumping to the file

x=0
phase = 0
values = 10
for j in range(9):
    for i in range(values):
        for k in range(1):
            stuff(x*np.pi*5*(k+1)/values+phase,j,k)
            #files[k].flush() #Always remember to flush
            x+=1
            time.sleep(1)
    phase+=np.pi/8

#with h5py.File("chaseCheck.h5","a") as f: #Open the file.
#    dset = f.get("Sweep0/table") #Get the table as a dataset from the file currently open.

#hdf.close()
#f.close()

import pandas as pd
import os
import time

#This class handles the saving for the loader to be able to recognise and use live each hdf5 file. It saves as a pandas
#dataframe and will create a dataset called "dataStruct" that holds information about the file such as the names of the
#sweeps as it turns out pandas.keys() methods are incredibly slow.
#If this is not used, there's a high probability that using the graphing system will cause your data to become corrupted.
class HDF5Saver():
    def __init__(self):
        self.sweeps = [] #Holds previously saved sweep names to check for unique ones.

    #Creates a new dictionary and makes sure each entry is a list, otherwise pandas breaks.
    #This is then converted into a pandas dataframe.
    #holdFile is a lock that prevents the graphing system for trying to read from the file while it's being written to.
    #A new HDFStore is created and appended to. If the sweep name given is new then it will add that into dataStruct first.
    #The opened files are then closed and the lock file removed.

    #Expected variables:
    #fileName: The name of the file, expected to be in the same place as the script is run. No idea how it'll react otherwise.
    #data: A dictionary with {name: [data]}. If data is not list then it will be converted to one.
    #location: The name of the sweep inside the HDF5 file.
    def save(self,fileName,data,location):
        tableStructure = {}
        for key in data.keys():
            tableStructure[key] = [key]
            if type(data[key]) is not list:
                data[key] = [data[key]]

        df = pd.DataFrame(data)
        struct = {"name": [location]}
        dfStruct = pd.DataFrame(struct)

        holdFile = os.getcwd()+"\hold\\"+fileName.split(".")[0]+".txt"
        f = open(holdFile,"w")
        while not os.path.exists(holdFile):
            time.sleep(0.001)
        pandaFile = pd.HDFStore(fileName,mode="a")
        if location not in self.sweeps:
            self.sweeps.append(location)
            pandaFile.append("dataStruct",dfStruct,format="table",data_columns = True)

        pandaFile.append(location, df, format='table', data_columns = True)
        pandaFile.close()
        f.close()
        os.remove(holdFile)

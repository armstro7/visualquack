Currently this is known to work using PyCharm 2018.1.2 (Edu) from this link: https://www.jetbrains.com/pycharm/download/#section=windows

The compiler used to create this was Anaconda for python 2.7 found here: https://www.anaconda.com/download/

Once you open up this project in PyCharm you will need to select this interpreter when prompted at the top of the screen. It will be found in C:\[Username]\ProgramData\anaconda2 

Don't close this screen, if on the right hand side there's a glowing circle, click it to turn it off. Then click the plus symbol and search for "pyqtgraph". On the bottom left will be a button to install this package. Do this. 

The file "savingToHDF5.py" is an example program that should demonstrate the format that data should be saved in. In HDF5 it will be group -> dataset named "table". data in def stuff() is a dictionary with the 'data' and 'count' being the names of a column of data, and attached is a list of the values that will be appended to the file. 

arbitraryCurves10WalkSettings.py is the program for the graphing. This will be able to read any file of the format above. Add file will open up a file dialogue for you to select the file that you want to load the data from. Click the dropdown button on the left will show all of your datasets. Here you can select your X and Y variables. Clicking the "live" or "active" checkboxes will plot a line graph with these variables. You can change them at any time and it will automatically change the graph. 


The code is commented as thoroughly as I believe I can make it, but if there are any questions email me at samarmstrong111@outlook.com
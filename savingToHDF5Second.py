import numpy as np
import HDF5Saver
import time

#Just created some data with a tonne of data columns, as those are what slows down the saving and loading the most.
x=0
phase = 0
values = 10

#Saver is created before any saving is done. If HDF5 file already exists, there may be problems. The saver only works
#for one file at the moment but may be extended in HDF5Saver using a dictionary instead of a list. It is also expected
#that the fileName is unique, but it will not delete anything.
saver = HDF5Saver.HDF5Saver()
for j in range(9):
    for i in range(values):
        data = {'data': [np.sin(x*np.pi*5*values+phase)], 'count': [x],'data1': [np.sin(x*np.pi*5*values+phase)], 'count1': [x],'data2': [np.sin(x*np.pi*5*values+phase)],
                'count2': [x],'data3': [np.sin(x*np.pi*5*values+phase)], 'count3': [x], 'data4': [np.sin(x*np.pi*5*values+phase)], 'count4': [x],'data5': [np.sin(x*np.pi*5*values+phase)],
                'count5': [x],'data6': [np.sin(x*np.pi*5*values+phase)], 'count6': [x],'data7': [np.sin(x*np.pi*5*values+phase)], 'count7': [x], 'data8': [np.sin(x*np.pi*5*values+phase)],
                'count8': [x],'data9': [np.sin(x*np.pi*5*values+phase)], 'count9': [x],'data10': [np.sin(x*np.pi*5*values+phase)], 'count10': [x],'data11': [np.sin(x*np.pi*5*values+phase)],
                'count11': [x]}
        saver.save("chaseCheck7.h5",data,"sweep"+str(j)) #Here is the call to save. Described in HDF5Saver
        x+=1
        time.sleep(1)
    print "Sweep:",j
    phase+=np.pi/8
